<?php

use App\Models\Cart;
use App\Models\Cart_Product;
use App\Models\Order;
use App\Models\Product;
use App\User;
use Illuminate\Database\Seeder;

class CartsOrders extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $products = Product::all();
        $users = User::all();

        foreach ($users as $user) {
            $cart = factory(Cart::class)->create(['user_id' => $user->id]);

            factory(Cart_Product::class, rand(1, 5))->make()->each(function (
                Cart_Product $cartProduct
            ) use ($products, $cart) {
                $cartProduct->product_id = $products->random()->id;
                $cartProduct->cart_id = $cart->id;
                $cartProduct->save();
            });

            if (rand(0, 5)) {
                factory(Order::class)->create([
                    'cart_id' => $cart->id,
                    'address' => str_random(),
                    'tel' => $user->tel,
                    'user_id' => $user->id,
                ]);
            }
        }
    }
}
