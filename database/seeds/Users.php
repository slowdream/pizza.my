<?php

use Illuminate\Database\Seeder;

class Users extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\User::class)->create([
            'name' => 'Admin',
            'tel' => '9996241313',
            'password' => bcrypt('password'),
        ])->setAttribute('role_id', \App\User::ROLE_ADMIN);

        factory(App\User::class, 29)->create();
    }
}
