<?php

use Illuminate\Database\Seeder;
use App\Models\Feedback as FeedbackModel;

class Feedback extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = \App\User::all('id');

        factory(FeedbackModel::class, 70)->make()->each(function (
            FeedbackModel $feedback
        ) use ($users) {
            $feedback->user_id = $users->random()->id;
            $feedback->status_id = rand(0, 2);
            $feedback->save();
        });
    }
}
