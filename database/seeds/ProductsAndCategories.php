<?php

use Illuminate\Database\Seeder;

class ProductsAndCategories extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = factory(App\Models\Category::class, 4)->create();

        foreach ($categories as $category) {
            factory(App\Models\Product::class, rand(5, 50))->create([
                'category_id' => $category->id,
            ]);
        }
    }
}
