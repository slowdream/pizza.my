<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    public function run()
    {
        $this->call(Users::class);
        $this->call(ProductsAndCategories::class);
        $this->call(CartsOrders::class);
        $this->call(Feedback::class);
        $this->call(Pages::class);
    }
}
