<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Product::class, function (Faker $faker) {
    return [
        'name' => $faker->streetName,
        'images' => array_map(function () use ($faker) {
            return $faker->imageUrl();
        }, array_fill(0, rand(1, 5), 0)),
        'price' => $faker->numberBetween(100, 2000),
        'weight' => $faker->numberBetween(100, 2000),
        'description' => $faker->text(rand(20, 200)),
    ];
});
