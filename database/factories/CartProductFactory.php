<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Cart_Product::class, function (Faker $faker) {
    return [
        'count' => rand(1, 5),
    ];
});
