<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Cart::class, function (Faker $faker) {
    return [
        'code' => random_int(100, 100000),
    ];
});
