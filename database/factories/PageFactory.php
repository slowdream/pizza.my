<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Page::class, function (Faker $faker) {
    return [
        'title' => $faker->dayOfWeek,
        'body' => $faker->text,
    ];
});
