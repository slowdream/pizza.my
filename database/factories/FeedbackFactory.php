<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Feedback::class, function (Faker $faker) {
    return [
        'title' => $faker->title,
        'body' => $faker->text(rand(100, 500)),
        'status_id' => rand(0, 2),
    ];
});
