<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/html', function () {
    return view('cart');
})->name('page');

Route::get('/', 'HomeController@index')->name('home');
Route::get('/feedback', 'FeedbackController@index')->name('feedback');
Route::post('/feedback/add', 'FeedbackController@add')->name('feedback.add');

Route::group(['prefix' => 'cart'], function () {
    Route::get('/', 'CartController@index');

    Route::put('/item/{productId}', 'CartController@add')->name('addToCart');
});

Route::get('/html', function () {
    return view('cart');
})->name('page');




Route::get('/menu/{slug}', function (string $slug, \App\Models\Category $category) {
    $category = $category->findBySlugOrFail($slug);
    return view(
        'category',
        [
            'name' => $category->name,
            'products' => $category->products,
        ]
    );
})->name('category');

Auth::routes();
