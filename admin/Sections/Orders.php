<?php

namespace Admin\Sections;

use App\Models\Order;
use AdminColumn;
use AdminDisplay;
use AdminForm;
use AdminFormElement;
use App\Models\Product;
use App\User;
use SleepingOwl\Admin\Contracts\Display\DisplayInterface;
use SleepingOwl\Admin\Contracts\Form\FormInterface;
use SleepingOwl\Admin\Contracts\Initializable;
use SleepingOwl\Admin\Section;

class Orders extends Section implements Initializable
{
    /** @var Order */
    protected $model;

    /**
     * Initialize class.
     */
    public function initialize()
    {
        // Добавление пункта меню и счетчика кол-ва записей в разделе
        $this->addToNavigation($priority = 500, function () {
            return Order::count();
        });
    }

    public function onDisplay(): DisplayInterface
    {
        return AdminDisplay::table()
            ->setHtmlAttribute('class', 'table-primary')
            ->setColumns(
                AdminColumn::text('id', '#')->setWidth('30px'),
                AdminColumn::relatedLink('user.tel', 'Покупатель'),
                AdminColumn::text('status', 'Статус'),
                AdminColumn::text('payed', 'Оплата'),
                AdminColumn::custom('Сумма', function (Order $order): int {
                    $sum = 0;
                    $cart_products = $order->cart->products;

                    foreach ($cart_products as $cart_product) {
                        $sum += $cart_product->count * $cart_product->product->price;
                    }
                    return $sum;
                })
            )->paginate(20);
    }

    public function onEdit(int $id = null): FormInterface
    {
        //TODO: Реализовать вывод информации о заказе без возможности редактировать
        return AdminForm::panel()->addBody([]);
    }
}
