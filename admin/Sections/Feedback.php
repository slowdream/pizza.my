<?php

namespace Admin\Sections;

use AdminColumn;
use AdminDisplay;
use AdminForm;
use AdminFormElement;
use App\Models\Feedback as FeedbackModel;
use App\User;
use SleepingOwl\Admin\Contracts\Display\DisplayInterface;
use SleepingOwl\Admin\Contracts\Form\FormInterface;
use SleepingOwl\Admin\Contracts\Initializable;
use SleepingOwl\Admin\Section;

class Feedback extends Section implements Initializable
{
    /** @var FeedbackModel */
    protected $model;

    /**
     * Initialize class.
     */
    public function initialize()
    {
        $this->addToNavigation($priority = 500, function () {
            return FeedbackModel::count();
        });
    }

    public function onDisplay(): DisplayInterface
    {
        return AdminDisplay::table()
            ->setHtmlAttribute('class', 'table-primary')
            ->setColumns(
                AdminColumn::text('id', '#')->setWidth('30px'),
                AdminColumn::relatedLink('user.name', 'Автор'),
                AdminColumn::text('title', 'Заголовок'),
                AdminColumn::text('status', 'Статус')
            )->paginate(20);
    }

    public function onEdit(int $id = null): FormInterface
    {
        return AdminForm::panel()->addBody([
            AdminFormElement::text('title', 'Заголовок')->required(),
            AdminFormElement::wysiwyg('body', 'Текст записи')->required(),
            AdminFormElement::select('user_id', 'Автор', User::class)
                ->setDisplay('tel')
                ->setLoadOptionsQueryPreparer(function ($element, $query) {
                    return $query->where('role_id', User::ROLE_USER);
                }),
            AdminFormElement::select('status_id', 'Статус', FeedbackModel::STATUS_MAP),
        ]);
    }

    public function onCreate(): FormInterface
    {
        return $this->onEdit(null);
    }

    /**
     * Переопределение метода содержащего заголовок создания записи
     *
     * @return string|\Symfony\Component\Translation\TranslatorInterface
     */
    public function getCreateTitle()
    {
        return 'Добавление Записи';
    }
}
