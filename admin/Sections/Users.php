<?php

namespace Admin\Sections;

use App\User;
use AdminColumn;
use AdminDisplay;
use AdminForm;
use AdminFormElement;
use SleepingOwl\Admin\Contracts\Display\DisplayInterface;
use SleepingOwl\Admin\Contracts\Form\FormInterface;
use SleepingOwl\Admin\Contracts\Initializable;
use SleepingOwl\Admin\Section;

class Users extends Section implements Initializable
{
    /** @var User */
    protected $model;

    /**
     * Initialize class.
     */
    public function initialize()
    {
        $this->addToNavigation($priority = 500, function () {
            return User::count();
        });
    }

    public function onDisplay(): DisplayInterface
    {
        return AdminDisplay::table()
            ->setHtmlAttribute('class', 'table-primary')
            ->setColumns(
                AdminColumn::text('id', '#')->setWidth('30px'),
                AdminColumn::text('name', 'Имя'),
                AdminColumn::text('tel', 'Телефон'),
                AdminColumn::email('email', 'Почта')
            )->paginate(20);
    }

    public function onEdit(int $id = null): FormInterface
    {
        return AdminForm::panel()->addBody([
            AdminFormElement::text('name', 'Имя')->required(),
            AdminFormElement::text('tel', 'Телефон')->required(),
            AdminFormElement::text('email', 'Почта')->required(),
            AdminFormElement::select('role_id', 'Роль', User::ROLES_MAP)->required(),
            AdminFormElement::password('password', 'Пароль')->required(),
        ]);
    }

    public function onCreate(): FormInterface
    {
        return $this->onEdit(null);
    }

    /**
     * Переопределение метода содержащего заголовок создания записи
     *
     * @return string|\Symfony\Component\Translation\TranslatorInterface
     */
    public function getCreateTitle()
    {
        return 'Добавление Пользователя';
    }
}
