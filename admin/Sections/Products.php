<?php

namespace Admin\Sections;

use App\Models\Category;
use App\Models\Product;
use AdminColumn;
use AdminDisplay;
use AdminForm;
use AdminFormElement;
use SleepingOwl\Admin\Contracts\Display\DisplayInterface;
use SleepingOwl\Admin\Contracts\Form\FormInterface;
use SleepingOwl\Admin\Contracts\Initializable;
use SleepingOwl\Admin\Section;

class Products extends Section implements Initializable
{
    /** @var Product */
    protected $model;

    /**
     * Initialize class.
     */
    public function initialize()
    {
        // Добавление пункта меню и счетчика кол-ва записей в разделе
        $this->addToNavigation($priority = 500, function () {
            return Product::count();
        });
    }

    public function onDisplay(): DisplayInterface
    {
        return AdminDisplay::table()
            ->setHtmlAttribute('class', 'table-primary')
            ->setColumns(
                AdminColumn::text('id', '#')->setWidth('30px'),
                AdminColumn::text('name', 'Имя'),
                AdminColumn::text('price', 'Цена'),
                AdminColumn::text('category.name', 'Категория')
            )->paginate(20);
    }

    public function onEdit(int $id = null): FormInterface
    {
        return AdminForm::panel()->addBody([
            AdminFormElement::text('name', 'Имя')->required(),
            AdminFormElement::images('images', 'Изображения'),
            AdminFormElement::text('price', 'Цена')->required(),
            AdminFormElement::text('weight', 'Вес')->required(),
            AdminFormElement::text('description', 'Описание')->required(),
            AdminFormElement::select('category_id', 'Категория')
                ->setModelForOptions(Category::class, 'name')
                ->required(),
        ]);
    }

    public function onCreate(): FormInterface
    {
        return $this->onEdit(null);
    }

    /**
     * Переопределение метода содержащего заголовок создания записи
     *
     * @return string|\Symfony\Component\Translation\TranslatorInterface
     */
    public function getCreateTitle()
    {
        return 'Добавление Товара';
    }
}
