<footer>
    <div class="content">
        <ul>
            <li><h4 class="title">О нас</h4></li>
            <li><a href="#">Вакансии</a></li>
            <li><a href="#">Контакты</a></li>
            <li><a href="#">Мобильная версия</a></li>
            <li><br></li>
            <li>
                <p>
                    Доставляем пиццу, роллы, домашнюю кухню по Санкт-Петербургу с 2013
                    года.
                </p>
                <span class="copyright">© pizzaservice.ru 2013-2019 </span>
            </li>
        </ul>

        <ul>
            <li><h4 class="title">Информация</h4></li>
            <li><a href="#">Pizza Service <br> Никольское. <br> Советский пр, 227</a></li>
            <li><br></li>
            <li>
                <span class="socials">
                    <a href="#"><i class="fab fa-vk"></i></a>
                    <a href="#"><i class="fab fa-instagram"></i></a>
                </span>
            </li>
            <li>+ 7(495)-43-23</li>
            <li><a href="#">Заказать звонок</a></li>
        </ul>

        <ul>
            <li><h4 class="title">Доставка</h4></li>
            <li><a href="#">Доставка и оплата</a></li>
            <li><a href="#">Доставка на станциях метро</a></li>
            <li><br></li>
            <li><h4 class="title">Нашли ошибку на сайте?</h4>
            <li><a href="#">Сообщите нам об этом</a></li>
        </ul>

        <ul>
            <li><h4 class="title">Клиентам</h4></li>
            <li><a href="#">Отзывы</a></li>
            <li><a href="#">Публичная оферта</a></li>
            <li><a href="#">Политика конфиденциальности</a></li>
        </ul>
    </div>
</footer>

<div role="alert" aria-live="assertive" aria-atomic="true" class="toast add2cart hide"
     data-delay="2000">
    <div class="toast-header">
        <strong class="mr-auto">Pizza!</strong>
        {{--        <small>11 mins ago</small>--}}
        <button type="button" class="ml-2 mb-1 close" data-dismiss="toast"
                aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    <div class="toast-body">
        Товар успешно добавлен в корзину
    </div>
</div>

<script src="https://code.jquery.com/jquery-3.4.0.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bs-stepper/dist/js/bs-stepper.min.js"></script>

<script src="/js/libs/owlcarousel2/owl.carousel.min.js"></script>
<script src="/js/libs/jquery.maskedinput.min.js"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css"
      rel="stylesheet"/>
<link href="https://cdn.jsdelivr.net/npm/bs-stepper/dist/css/bs-stepper.min.css" rel="stylesheet"/>
<script
    src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>

<script src="{{ asset('js/libs/bootstrap.bundle.js') }}" defer></script>
<script src="{{ asset('js/libs/bootstrap-input-spinner.js') }}" defer></script>
<script src="{{ asset('js/app.js') }}" defer></script>
