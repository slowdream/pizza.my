@extends('layouts.app')

@section('content')
    <div class="wrapper">
        <section class="products-carousel">
            <h2 class="title">Лучшие предложения</h2>
            <div class="owl-carousel">
                @foreach ($bestProducts as $product)
                    @include('chunks.small_card', $product)
                @endforeach
            </div>
        </section>
    </div>
    <section class="delivery-map">
        <div class="wrapper">
            <h2 class="title">Круглосуточная бесплатная доставка</h2>
            <h3 class="sub-title">при заказе от 500 рублей</h3>
            <div class="container">
                <div class="row">
                    <div id="delivery-map" class="col-8">
                        {{--                        <img src="/img/map.jpg" alt="map"--}}
                        {{--                             style="height: 100%;">--}}

                        <!— BEGIN: page —>
                        <div id="yandex-map" style="height:450px;">
                            <script type="text/javascript" charset="utf-8" async
                                    src="https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3A534d3bc265e6f037ab04252e44fc4951f7d8aa2527a5d3ed8cd8a612adb753d8&amp;width=100%25&amp;height=400&amp;lang=ru_RU&amp;scroll=true"></script>
                        </div>
                        <!— END: page —>
                    </div>
                    <div class="col-4">
                        <label for="addr">Адрес доставки</label>
                        <div class="input_wrapper">
                            <input type="text" id="addr">
                            <button><i class="fas fa-location-arrow"></i></button>
                        </div>
                        <div id="delivery_time" class="delivery-time">
                            <div class="delivery-time-clock">
                                <img src="/img/timer.png" alt="timer">
                            </div>
                            <span class="delivery-time-text">
                                Доставка приедет к вам в течение 45 минут
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="our-clients">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <h2 class="title">Наши клиенты</h2>
                </div>
            </div>
        </div>
    </section>

    <section class="feedback-list">
        <div class="wrapper">
            <div class="container">
                <div class="row">
                    <div class="col">
                        <h2 class="title">Отзывы</h2>
                    </div>
                </div>

                <div class="row">
                    @foreach ($feedbackList as $feedback)
                        @include('chunks.small_feedback', $feedback)
                    @endforeach
                </div>

                <div class="row justify-content-center">
                    <a href="{{route('feedback')}}" class="btn">ОСТАВИТЬ ОТЗЫВ</a>
                </div>
            </div>
        </div>
    </section>
@endsection
