@extends('layouts.app')

@section('content')
    <section class="feedback-list">
        <div class="wrapper">
            <div class="container">
                <h2 class="title">Отзывы</h2>

                <div class="row">
                    @foreach ($feedbackList as $feedback)
                        @include('chunks.small_feedback', $feedback)
                    @endforeach
                </div>

                <div class="row justify-content-center">
                    {{ $feedbackList->onEachSide(1)->links() }}
                </div>
            </div>
        </div>
    </section>
    <section class="feedback-add">
        <div class="wrapper">
            <div class="container">
                <form action="{{ route('feedback.add') }}">
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="FeedbackN">Имя</label>
                            <input type="text" id="FeedbackN" class="form-control" name="name" placeholder="Имя">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="FeedbackF">Фамилия</label>
                            <input type="text" id="FeedbackF" class="form-control" placeholder="Фамилия">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="FeedbackT">Текст отзыва</label>
                        <textarea type="text" id="FeedbackT" class="form-control" placeholder="Не стесняйтесь ;)">
                        </textarea>
                    </div>

                    <button type="submit" class="btn btn-primary">Отправить</button>
                </form>
            </div>
        </div>
    </section>
@endsection
