<!-- Button trigger modal -->
<button type="button" class="btn btn-primary" data-toggle="modal"
        data-target="#LoginModal">
    ЛОгин
</button>

<!-- Modal -->
<div class="modal fade" id="LoginModal" tabindex="-1" role="dialog"
     aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Войти</h5>
                <button type="button" class="close" data-dismiss="modal"
                        aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="LoginForm" action="{{ route('login') }}">
                <div class="modal-body">
                    <div class="form-group">
                        <label for="LoginTel">Номер телефона</label>
                        <input id="LoginTel" type="tel" name="tel" class="form-control"
                               aria-describedby="telHelp" placeholder="+7 (999) 666-1212">
                    </div>
                    <div class="form-group">
                        <label for="LoginPassword">Пароль</label>
                        <input type="password" class="form-control" id="LoginPassword"
                               placeholder="Password" name="password">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Войти</button>
                </div>
            </form>

        </div>
    </div>
</div>


<!-- Button trigger modal -->
<button type="button" class="btn btn-primary" data-toggle="modal"
        data-target="#RegisterModal">
    Регистрация
</button>

<!-- Modal -->
<div class="modal fade" id="RegisterModal" tabindex="-1" role="dialog"
     aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Регистрация</h5>
                <button type="button" class="close" data-dismiss="modal"
                        aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="RegisterForm" action="{{ route('register') }}">
                <div class="modal-body">
                    <div class="form-group">
                        <label for="RegName">Имя</label>
                        <input id="RegName" type="text" name="name" class="form-control"
                               aria-describedby="nameHelp" placeholder="Имя" required>
                    </div>
                    <div class="form-group">
                        <label for="RegTel">Номер телефона</label>
                        <input id="RegTel" type="tel" name="tel" class="form-control"
                               aria-describedby="telHelp" placeholder="+7 (999) 666-1212"
                               required>
                    </div>
                    <div class="form-group">
                        <label for="RegPassword">Пароль</label>
                        <input type="password" class="form-control" id="RegPassword"
                               placeholder="Password" required>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">
                        Зарегистрироваться
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
