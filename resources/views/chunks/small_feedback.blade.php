@php
    /** @var \App\Models\Feedback $feedback */
@endphp

<div class="col">
    <div class="small-feedback">
        <div class="photo-wrapper">
            {{ $feedback->img }}
            <img class="photo" src="/img/face.jpg" alt="photo">
        </div>
        <span class="name">
            {{ $feedback->title }}
            {{-- <i class="fab fa-instagram"></i> @delovery --}}
        </span>
        <div class="text">
            {{ $feedback->body }}
        </div>
    </div>
</div>
