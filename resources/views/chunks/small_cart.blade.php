@php
    /** @var \App\Contracts\Services\Cart $cart */
$items = $cart->getItems();
@endphp

<a href="/cart" id="SmallCart" class="small_cart">
    <i class="fas fa-shopping-cart" style="color: #ffffff;"></i>
    <span class="price">{{ $cart->getTotalPrice() }} р.</span>

    <div class="small_cart_popup">
        <span class="title">Корзина</span>
        <table>
            @foreach($items as $item)
                <tr>
                    <td>
                        <span class="name">{{ $item['name'] }}</span>
                        <br>
                        <span class="weight">{{ $item['weight'] }}</span>
                    </td>
                    <td>
                        <span class="price">
                            {{ $item['price'] }} <i class="fas fa-ruble-sign"></i>
                        </span>
                    </td>
                </tr>
            @endforeach
            <tr>
                <td><span class="name">Сумма</span></td>
                <td>
                    <span class="price">
                        {{ $cart->getTotalPrice() }} <i class="fas fa-ruble-sign"></i>
                    </span>
                </td>
            </tr>
            <caption>Доставка: бесплатно</caption>
        </table>

    </div>
</a>
