@php
    /** @var \App\Models\Product $product */
@endphp

<div class="small-card">
    <div class="image">
        <img src="{{ $product->images->first() }}" alt="">
    </div>
    <div class="content">
        <span class="name">{{ $product->name }}</span>
        <span class="price">{{ $product->price }} <i class="fas fa-ruble-sign"></i></span>
        <p class="structure">
            {{ $product->description }}
        </p>
        <span class="weight">{{ $product->weight }}</span>
        <a href="#" class="btn addToCart" data-id="{{ $product->id }}">
            <i class="fas fa-shopping-cart" style="color: #ffffff;"></i>
            В КОРЗИНУ
        </a>
    </div>
</div>
