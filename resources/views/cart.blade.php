@extends('layouts.app')

@section('content')
    <section class="container">
        <div class="bs-stepper">
            <div class="row">
                <div class="col-6"><h2>Корзина</h2></div>
                <div class="col-6 bs-stepper-header" role="tablist">
                    <!-- your steps here -->
                    <div class="step" data-target="#step1">
                        <button type="button" class="step-trigger" role="tab"
                                aria-controls="step1" id="step1-trigger">
                            <span class="bs-stepper-circle">1</span>
                            <span class="bs-stepper-label">Корзина</span>
                        </button>
                    </div>
                    <div class="line"></div>
                    <div class="step" data-target="#step2">
                        <button type="button" class="step-trigger" role="tab"
                                aria-controls="step2" id="step2-trigger">
                            <span class="bs-stepper-circle">2</span>
                            <span class="bs-stepper-label">Оформление заказа</span>
                        </button>
                    </div>
                    <div class="line"></div>
                    <div class="step" data-target="#step3">
                        <button type="button" class="step-trigger" role="tab"
                                aria-controls="step3" id="step3-trigger">
                            <span class="bs-stepper-circle">3</span>
                            <span class="bs-stepper-label">Заказ готовится</span>
                        </button>
                    </div>
                </div>
            </div>
            <div class="row">
                <form class="col bs-stepper-content">
                    <!-- your steps content here -->
                    <div id="step1" class="content" role="tabpanel"
                         aria-labelledby="step1-trigger">
                        <table class="table product_table">
                            <tbody>
                            @foreach($cartProducts as $product)
                                <tr>
                                    <td><i class="fas fa-shopping-cart"></i></td>
                                    <td class="name">{{ $product['name'] }}</td>
                                    <td class="weight">{{ $product['weight'] }}</td>
                                    <td class="spinner">
                                        <input type="number"
                                               value="{{ $product['count'] }}" min="0"
                                               max="100"
                                               step="1">
                                    </td>
                                    <td class="price">{{ $product['price'] }} р</td>
                                    <td>
                                        <a href="#" class="close">
                                            <span aria-hidden="true">&times;</span>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <p>Сумма заказа: {{ $cart->getTotalPrice() }}р </p>
                        <a href="#" class="btn reverse">Вернуться в меню</a>
                        <button class="btn float-right"
                                onclick="stepper.next(); return false">
                            Заказать
                        </button>
                    </div>
                    <div id="step2" class="content" role="tabpanel"
                         aria-labelledby="step2-trigger">

                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="inputName">Имя</label>
                                <input type="text" name="name" value="@auth {{ Auth::user()->name }} @endauth"
                                       class="form-control"
                                       id="inputName" placeholder="Имя">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="inputTel">Номер телефона</label>
                                <input type="tel" name="tel" value="@auth {{ Auth::user()->tel }} @endauth" class="form-control"
                                       id="inputTel" placeholder="+7 (999) 666-1212">
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="inputPlace">Адрес доставки</label>
                                <textarea name="place" class="form-control"
                                          id="inputPlace"
                                          placeholder="Адрес доставки"></textarea>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="inputComment">Комментарий</label>
                                <textarea name="comment" class="form-control"
                                          id="inputComment"
                                          placeholder="Комментарий"></textarea>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="timeDelivery">Время доставки</label>
                                <select name="time" class="form-control"
                                        id="timeDelivery">
                                    <option value="0">Сразу по готовности</option>
                                    <option value="15">+15 минут</option>
                                    <option value="30">+30 минут</option>
                                </select>
                            </div>
                        </div>


                        <div class="form-row">
                            <fieldset class="form-group col">
                                <legend>Оплата</legend>
                                <div class="form-check">
                                    <input class="form-check-input" type="radio"
                                           name="gridRadios" id="gridRadios1"
                                           value="option1" checked>
                                    <label class="form-check-label" for="gridRadios1">
                                        Наличными при доставке
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input" type="radio"
                                           name="gridRadios" id="gridRadios2"
                                           value="option2">
                                    <label class="form-check-label" for="gridRadios2">
                                        Картой при доставке
                                    </label>
                                </div>
                                <div class="form-check disabled">
                                    <input class="form-check-input" type="radio"
                                           name="gridRadios" id="gridRadios3"
                                           value="option3">
                                    <label class="form-check-label" for="gridRadios3">
                                        Картой на сайте
                                    </label>
                                </div>
                            </fieldset>
                        </div>

                        <div class="form-group row">
                            <div class="col-12">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox"
                                           id="gridCheck1">
                                    <label class="form-check-label" for="gridCheck1">
                                        Соглашаюсь на обработку персональных данных по
                                        условиям
                                        <a href="#">пользовательского соглашения</a>
                                    </label>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox"
                                           id="gridCheck2">
                                    <label class="form-check-label" for="gridCheck2">
                                        Хочу получать лучшие предложения
                                    </label>
                                </div>
                            </div>
                        </div>


                        <button class="btn reverse float-left"
                                onclick="stepper.next(); return false">
                            Заказать
                        </button>
                        <button class="btn float-right btn-primary"
                                onclick="stepper.next(); return false">
                            Оформить заказ за 860р.
                        </button>

                    </div>
                    <div id="step3" class="content" role="tabpanel"
                         aria-labelledby="step3-trigger">
                        <p>Ваш заказ готовится!</p>
                    </div>
                </form>
            </div>
        </div>
    </section>
@endsection

