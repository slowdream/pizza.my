@extends('layouts.app')

@section('content')
    <div class="wrapper">
        <div class="sort">
            <select class="js-example-basic-single" name="sort">
                <option value="AL">По категориям</option>
                <option value="WY">По цене</option>
            </select>
        </div>
        <h2 class="title">{{ $name }}</h2>

        <section class="products_list">
            @each('chunks.small_card', $products, 'product')
        </section>
    </div>
@endsection
