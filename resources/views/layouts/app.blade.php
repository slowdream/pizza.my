<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
@include('parts.head')

<body>

@include('parts.header')

@if (session('status'))
    <div class="alert alert-success" role="alert">
        {{ session('status') }}
    </div>
@endif

<main>
    @yield('content')
</main>

@include('chunks.login_and_register_form')
@include('parts.footer')

</body>

</html>
