@extends('layouts.app')

@section('content')
    <section class="feedback-list">
        <div class="wrapper">
            <div class="container-fluid">
                <h2 class="title">{{ $page->title }}</h2>

                <div class="row">
                    <div class="col-12">
                        {{ $page->body }}
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
