<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class PagesAvailableTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testStatuses()
    {
        // Main page 200
        $this->get('/')
            ->assertStatus(200);

        // 404
        $this->get('/' . str_random())
            ->assertStatus(404);
    }
}
