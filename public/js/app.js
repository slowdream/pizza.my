$(document).ready(function () {
    $("#hamburger").click(function () {
        $(this).toggleClass("is-active");
    });

    $(".owl-carousel").owlCarousel({
        loop: true,
        items: 5,
        margin: 45,
        nav: true,
        responsive: {
            0: {
                items: 1
            },
            1300: {
                items: 4
            }
        },
    });

    $('.js-example-basic-single').select2({
        minimumResultsForSearch: Infinity,
        width: '245px',
    });

    $('.addToCart').on('click', function () {
        let productId = $(this).data('id');
        $.ajax({
            url: '/cart/item/' + productId,
            type: 'PUT',
            headers: {
                'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (data) {
                $('#SmallCart').replaceWith(data);
                $('.toast.add2cart').toast('show')
            }
        });

        return false;
    });
    $("input[type='tel']").mask("+7 (999) 999-9999");

    $("input[type='number']").inputSpinner({
        decrementButton: "-", // button text
        incrementButton: "+", // ..
        groupClass: "", // css class of the resulting input-group
        buttonsClass: "btn-outline-secondary",
        buttonsWidth: "auto",
        textAlign: "center",
        autoDelay: 500, // ms holding before auto value change
        autoInterval: 100, // speed of auto value change
        boostThreshold: 10, // boost after these steps
        boostMultiplier: "auto", // you can also set a constant number as multiplier
        locale: null // the locale for number rendering; if null, the browsers language is used
    });


    $('#LoginForm').on('submit', function(e){
        e.preventDefault();
        $.ajax({
            url: $(this).attr('action'),
            data: {
                tel: $('#LoginTel').val().substring(2).replace(/[^0-9]/gim,''),
                password: $('#LoginPassword').val()
            },
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            type: 'POST',
            dataType: 'JSON',
            success: function (html) {
                document.location.href="/";
            }
        });

        console.log('Submit');
    });

    $('#RegisterForm').on('submit', function(e){
        e.preventDefault();
        $.ajax({
            url: $(this).attr('action'),
            data: {
                name: $('#RegName').val(),
                tel: $('#RegTel').val().substring(2).replace(/[^0-9]/gim,''),
                password: $('#RegPassword').val()
            },
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            type: 'POST',
            dataType: 'JSON',
            success: function (html) {
                document.location.href="/";
            }
        });

        console.log('Submit');
    });
});

const stepper = new Stepper($('.bs-stepper')[0]);
