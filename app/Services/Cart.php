<?php

namespace App\Services;

use App\Models\Cart as CartModel;
use App\Models\Cart_Product;
use App\Models\Product;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Session\SessionManager;

class Cart implements \App\Contracts\Services\Cart
{
    /**
     * @var SessionManager
     */
    private $session;

    /**
     * @var int|null
     */
    private $userId;

    /**
     * @var CartModel
     */
    private $cartModel;

    /**
     * @var int
     */
    private $code;

    function __construct(Request $request, CartModel $cartModel)
    {
        $this->session = session();
        $this->userId = $request->user() ? $request->user()->id : null;
        $this->code = $this->session->get('cart.code');

        $this->cartModel = $cartModel->where(['code' => $this->getCode()])->first();

        if ($this->cartModel === null) {
            $this->createNewCart();
        }
    }

    public function getCode(): int
    {
        if (is_null($this->code)) {
            $this->createNewCart();
        }

        return $this->code;
    }

    public function clear(): void
    {
        $this->session->remove('cart');
        $this->cartModel->whereCode($this->getCode())->delete();
        $this->createNewCart();
    }

    /**
     * Сумма стоимостей всех позиций в корзине. Итоговая сумма на чеке.
     */
    public function getTotalPrice(): int
    {
        $total = $this->session->get('cart.total');

        if (is_null($total)) {
            /** @var Collection $cart */
            $cart = $this->cartModel->with([
                'products' => function ($query) {
                    $query->with('product');
                },
            ])
                ->whereCode($this->getCode())
                ->first();

            $total = $cart->products->sum(function ($item) {
                return $item->count * $item->product->price;
            });

            $this->session->put('cart.total', $total);
        }

        return $total;
    }

    /**
     * Суммарное количество единиц товара в заказе
     */
    public function getCount(): int
    {
        $count = $this->session->get('cart.count');

        if (is_null($count)) {
            $count = $this->cartModel->products()->count();

            $this->session->put('cart.count', $count);
        }

        return $count;
    }

    /**
     * Позиции заказа.
     *
     * @return array
     */
    public function getItems(): array
    {
        $products = $this->cartModel->products;
        $productIds = $products->pluck('product_id')->all();
        $productModels = Product::find($productIds)->all();

        return array_map(function (Product $product) use ($products) {
            $product = $product->toArray();

            $product['count'] = $products->where('product_id', $product['id'])->count();
            return $product;
        }, $productModels);
    }

    public function setQuantity(int $cartProductId, int $quantity): void
    {
        $product = $this->cartModel->products()->find($cartProductId);
        $product->count = $quantity;
        $product->save();

        $this->clearCounters();
    }

    public function addItem(int $productId, int $quantity = 1): void
    {
        /** @var Cart_Product $item */
        $item = $this->cartModel->products()->firstOrCreate([
            'product_id' => $productId,
            'cart_id' => $this->cartModel->id,
        ]);

        $this->setQuantity($item->id, $item->count + $quantity);
    }

    public function deleteItem(int $productId): void
    {
        $this->cartModel->products()->find($productId)->delete();

        $this->clearCounters();
    }

    private function createNewCart(): void
    {
        $this->code = random_int(100, 100000);

        // Временно.
        $this->cartModel = CartModel::firstOrCreate([
            'code' => $this->getCode(),
        ]);

        if ($this->userId) {
            $this->cartModel->user_id = $this->userId;
            $this->cartModel->save();
        }

        $this->session->put('cart.code', $this->getCode());

        $this->clearCounters();
    }

    private function clearCounters(): void
    {
        $this->session->remove('cart.count');
        $this->session->remove('cart.total');
    }
}
