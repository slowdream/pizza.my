<?php

namespace App\Services;

use Zelenin\SmsRu\Response\SmsResponse;

class Sms implements \App\Contracts\Services\Sms
{
    /** @var \Zelenin\SmsRu\Api */
    private $smsGate;

    public function __construct()
    {
        $apiId = config('sms.api');
        $this->smsGate = new \Zelenin\SmsRu\Api(
            new \Zelenin\SmsRu\Auth\ApiIdAuth($apiId)
        );
    }

    public function send(int $tel, string $text): SmsResponse
    {
        $sms = new \Zelenin\SmsRu\Entity\Sms($tel, $text);
        return $this->smsGate->smsSend($sms);
    }
}
