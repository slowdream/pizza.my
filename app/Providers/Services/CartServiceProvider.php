<?php

namespace App\Providers\Services;

use App;
use Illuminate\Support\ServiceProvider;

class CartServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        App::bind(
            App\Contracts\Services\Cart::class,
            App\Services\Cart::class
        );
    }
}
