<?php

namespace App\Providers\Services;

use App;
use Illuminate\Support\ServiceProvider;

class SmsServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        App::bind(
            'sms',
            App\Services\Sms::class,
            App\Contracts\Services\Sms::class
        );
    }
}
