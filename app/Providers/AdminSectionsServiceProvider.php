<?php

namespace App\Providers;

use SleepingOwl\Admin\Providers\AdminSectionsServiceProvider as ServiceProvider;

class AdminSectionsServiceProvider extends ServiceProvider
{
    /**
     * @var array
     */
    protected $sections = [
        \App\User::class => \Admin\Sections\Users::class,
        \App\Models\Product::class => \Admin\Sections\Products::class,
        \App\Models\Category::class => \Admin\Sections\Categories::class,
        \App\Models\Page::class => \Admin\Sections\Pages::class,
        \App\Models\Feedback::class => \Admin\Sections\Feedback::class,
        \App\Models\Order::class => \Admin\Sections\Orders::class,
    ];

    /**
     * Register sections.
     *
     * @param \SleepingOwl\Admin\Admin $admin
     * @return void
     */
    public function boot(\SleepingOwl\Admin\Admin $admin)
    {
//        \Debugbar::disable();

        parent::boot($admin);
    }
}
