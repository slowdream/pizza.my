<?php

namespace App;

use App\Models\Cart;
use App\Models\Feedback;
use App\Models\Order;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    const ROLE_USER = 0;
    const ROLE_ADMIN = 1;
    const ROLE_MODER = 2;

    const ROLES_MAP = [
        self::ROLE_USER => 'Покупатель',
        self::ROLE_ADMIN => 'Супер Админ',
        self::ROLE_MODER => 'Менеджер',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'tel',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    public function cart(): HasOne
    {
        return $this->hasOne(Cart::class);
    }

    public function orders(): HasMany
    {
        return $this->hasMany(Order::class);
    }

    public function feedback(): HasMany
    {
        return $this->hasMany(Feedback::class);
    }

    public function setTelAttribute($tel)
    {
        $this->attributes['tel'] = $tel;
    }
}
