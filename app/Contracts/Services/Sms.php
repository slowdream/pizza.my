<?php

namespace App\Contracts\Services;

interface Sms
{
    public function send(int $tel, string $text);
}
