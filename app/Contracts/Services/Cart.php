<?php

namespace App\Contracts\Services;

interface Cart
{
    public function getCode(): int;

    public function getTotalPrice(): int;

    public function getCount(): int;

    public function getItems(): array;

    public function setQuantity(int $cartProductId, int $quantity): void;

    public function clear(): void;

    public function addItem(int $productId, int $quantity = 1): void;

    public function deleteItem(int $cartProductId): void;
}
