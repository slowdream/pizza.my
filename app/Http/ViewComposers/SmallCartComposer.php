<?php

namespace App\Http\ViewComposers;

use App\Contracts\Services\Cart;
use Illuminate\Contracts\View\View;

class SmallCartComposer
{
    /**
     * @var Cart
     */
    protected $cart;


    public function __construct(Cart $cart)
    {
        $this->cart = $cart;
    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $view->with('cart', $this->cart);
    }
}
