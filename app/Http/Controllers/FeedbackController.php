<?php

namespace App\Http\Controllers;

use App\Models\Feedback;

class FeedbackController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    public function index(Feedback $feedback)
    {
        return view('feedback', ['feedbackList' => $feedback->paginate(3)]);
    }
}
