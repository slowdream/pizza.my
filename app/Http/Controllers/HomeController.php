<?php

namespace App\Http\Controllers;

use App\Models\Feedback;
use App\Models\Product;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
//        $this->middleware('auth');
    }

    public function index(Product $product, Feedback $feedback)
    {
        $data = [
            'bestProducts' => $product->find([10, 1, 12, 14, 15]),
            'feedbackList' =>
                $feedback->where('status_id', Feedback::STATUS_ON_MAIN)->limit(3)->get(),
        ];

        return view('home', $data);
    }
}
