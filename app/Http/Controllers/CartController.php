<?php

namespace App\Http\Controllers;

use App\Contracts\Services\Cart;

class CartController extends Controller
{
    public function index(Cart $cart)
    {
        if ($cart->getCount() === 0) {
            return redirect()->route('home');
        }

        return view('cart', ['cart' => $cart, 'cartProducts' => $cart->getItems()]);
    }

    public function add(int $productId, Cart $cart)
    {
        $cart->addItem($productId);

        return view('chunks.small_cart', ['cart' => $cart]);
    }
}
