<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Feedback extends Model
{
    protected $guarded = ['status_id'];

    const STATUS_OPEN = 0;
    const STATUS_PROCESSED = 1;
    const STATUS_ON_MAIN = 2;

    const STATUS_MAP = [
        self::STATUS_OPEN => 'Новый',
        self::STATUS_PROCESSED => 'Просмотренный',
        self::STATUS_ON_MAIN => 'На главной',
    ];

    public function getStatusAttribute(): string
    {
        return self::STATUS_MAP[$this->getAttribute('status_id')];
    }

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }
}
