<?php

namespace App\Models;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Product extends Model
{
    use Sluggable;

    protected $casts = [
        'images' => 'collection',
    ];

    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => ['category.slug', 'name'],
            ],
        ];
    }

    public function category(): BelongsTo
    {
        return $this->belongsTo(Category::class);
    }

    public function getImgAttribute()
    {
        return $this->images->first();
    }
}
