<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasOne;

class Order extends Model
{
    const STATUS_OPEN = 0;
    const STATUS_DELIVERING = 1;
    const STATUS_COMPLETED = 2;
    const STATUS_CANCELLED = 3;

    const PAYED_NONE = 0;
    const PAYED_ONLINE = 1;
    const PAYED_COURIER = 2;

    private const STATUS_MAP = [
        self::STATUS_OPEN => 'Открыт',
        self::STATUS_DELIVERING => 'Доставляется',
        self::STATUS_COMPLETED => 'Завершен',
        self::STATUS_CANCELLED => 'Отменен',
    ];

    private const PAYED_MAP = [
        self::PAYED_NONE => 'Не оплачен',
        self::PAYED_ONLINE => 'Онлайн',
        self::PAYED_COURIER => 'Наличными курьеру',
    ];

    protected $guarded = ['status_id', 'payed_id'];

    public function getStatusAttribute(): string
    {
        return self::STATUS_MAP[$this->getAttribute('status_id')];
    }

    public function getPayedAttribute(): string
    {
        return self::PAYED_MAP[$this->getAttribute('payed_id')];
    }

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function cart(): BelongsTo
    {
        return $this->belongsTo(Cart::class);
    }

    public function transaction(): HasOne
    {
        return $this->hasOne(Transaction::class);
    }
}
